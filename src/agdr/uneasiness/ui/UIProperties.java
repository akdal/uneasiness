package agdr.uneasiness.ui;

import java.io.*;
import java.util.Hashtable;
import java.awt.Color;
import java.awt.Font;

public class UIProperties {
	private Hashtable<String, Object> propertyTable = new Hashtable<String, Object>();
	
	public final static String TEXT_FONT = "font";
	public final static String LINEHIGHLIGHT_COLOR = "linehighlightColor";
	
	public static UIProperties getDefaultUIProperties(){
		UIProperties uip = new UIProperties();
		Font f = new Font("Monospaced", Font.PLAIN, 12);
		uip.setValue(TEXT_FONT, f);
		uip.setValue(LINEHIGHLIGHT_COLOR, Color.YELLOW);
		return uip;
	}
	
	public void store(File f) throws IOException{
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(propertyTable);
		oos.close();
		fos.close();
	}
	public void read(File f) throws IOException, ClassNotFoundException{
		FileInputStream fis = new FileInputStream(f);
		ObjectInputStream ois = new ObjectInputStream(fis);
		propertyTable = (Hashtable<String, Object>)ois.readObject();
		ois.close();
		fis.close();
	}
	public Object getValue(String str)
	{ return propertyTable.get(str); }
	public void setValue(String name, Object v)
	{ propertyTable.put(name,  v); }
}
