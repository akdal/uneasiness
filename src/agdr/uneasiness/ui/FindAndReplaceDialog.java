package agdr.uneasiness.ui;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class FindAndReplaceDialog extends JDialog{
	private MainPage mpage;
	
	private JPanel findReplacePanel;
	private JTextField findField;
	private JTextField replaceField;
	
	private JPanel buttonsPanel;
	private JButton findButton;
	private JButton replaceButton;
	private JButton replaceAllButton;
	private JButton closeButton;
	
	public FindAndReplaceDialog(MainPage mpage){
		super(mpage, "찾기/바꾸기");
		this.mpage = mpage;
		
		super.setLayout(new BorderLayout());
		
		_createFindReplacePanel();
		_createButtonsPanel();
		
		JPanel cpane = new JPanel();
		cpane.setLayout(new BorderLayout());
		cpane.setBorder(new EmptyBorder(10, 10, 10, 10));
		super.setContentPane(cpane);
		super.add(findReplacePanel, BorderLayout.NORTH);
		super.add(buttonsPanel, BorderLayout.SOUTH);
		super.pack();
	}
	
	void __gridbag(GridBagConstraints c, JComponent jc, GridBagLayout gl, JPanel jp){
		gl.setConstraints(jc, c);
		jp.add(jc);
	}
	void _createFindReplacePanel(){
		findReplacePanel = new JPanel();
		findReplacePanel.setSize(300, 100);
		GridBagLayout gl = new GridBagLayout();
		
		GridBagConstraints c= new GridBagConstraints();
		
		findField=  new JTextField();
		replaceField = new JTextField();
		
		c.gridx = 0; c.gridy = 0;
		c.gridwidth = 1;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0;
		__gridbag(c, new JLabel("Find : "), gl, findReplacePanel);
		c.gridx = 0; c.gridy = 1;
		__gridbag(c, new JLabel("Replace : "), gl, findReplacePanel);

		c.gridx = 1; c.gridy = 0;
		c.weightx = 1;
		__gridbag(c, findField, gl, findReplacePanel);
		c.gridx = 1; c.gridy = 1;
		__gridbag(c, replaceField, gl, findReplacePanel);
		
		findReplacePanel.setLayout(gl);
	}
	void _createButtonsPanel(){
		buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout());
		
		findButton = new JButton("찾기");
		replaceButton = new JButton("바꾸기");
		replaceAllButton = new JButton("모두 바꾸기");
		closeButton = new JButton("닫기");
		
		buttonsPanel.add(findButton);
		buttonsPanel.add(replaceButton);
		buttonsPanel.add(replaceAllButton);
		buttonsPanel.add(closeButton);
	}
	
	
	
	public static void main(String[] argv){
		new FindAndReplaceDialog(null).setVisible(true);
	}
}

