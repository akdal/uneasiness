package agdr.uneasiness.ui;

import javax.swing.*;
import java.io.*;

public class MakerInfoDialog extends JDialog{
	private JEditorPane jepane;
	
	public MakerInfoDialog() throws Exception{
		File f = new File("info.html");
		jepane = new JEditorPane("file:///" + f.getAbsolutePath());
		super.add(jepane);
		super.setSize(400, 400);
	}
}
