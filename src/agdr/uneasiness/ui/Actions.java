package agdr.uneasiness.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;

public class Actions {

	public static class OpenAction implements ActionListener{
		MainPage mpage;
		public OpenAction(MainPage mp)
		{ this.mpage 
			= mp; }
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub

			JFileChooser jfc= new JFileChooser();
			int retval = jfc.showOpenDialog(mpage);
			if(retval == JFileChooser.APPROVE_OPTION){
				mpage.openFile(jfc.getSelectedFile());
			}
		}
	}
	public static class UndoAction implements ActionListener{
		MainPage mpage;
		public UndoAction(MainPage mp)
		{ this.mpage 
			= mp; }
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			mpage.getCurrentViewer().undo();
		}
	}
	public static class RedoAction implements ActionListener{
		MainPage mpage;
		public RedoAction(MainPage mp)
		{ this.mpage 
			= mp; }
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			mpage.getCurrentViewer().redo();
		}
	}
	public static class CloseFileTabAction implements ActionListener{
		MainPage mpage;
		String tabname;
		public CloseFileTabAction(MainPage mp, String tabname)
		{ this.mpage 
			= mp; this.tabname = tabname; }
		public void actionPerformed(ActionEvent ae){
			mpage.closeFile(tabname);
		}
	}
	public static class FindAndReplaceAction implements ActionListener{
		MainPage mpage;
		FindAndReplaceDialog farDialog;
		public FindAndReplaceAction(MainPage mpage)
		{ this.mpage = mpage; 
			farDialog = new FindAndReplaceDialog(mpage);}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			farDialog.setVisible(true);
		}
	}
	public static class MakerInfoAction implements ActionListener{
		MakerInfoDialog midialog;
		public MakerInfoAction(){
			try{
				midialog= new MakerInfoDialog();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		public void actionPerformed(ActionEvent ae){
			midialog.setVisible(true);
		}
	}
}
