package agdr.uneasiness.ui;

import java.io.*;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeListener;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import static agdr.uneasiness.ui.Actions.*;

public class MainPage extends JFrame{
	private static final String APPLICATION_NAME = "Uneasiness";
	
	public OpenAction OPEN_ACTION = new OpenAction(this);
	public FindAndReplaceAction FIND_REPLACE_ACTION = new FindAndReplaceAction(this);
	public UndoAction UNDO_ACTION = new UndoAction(this);
	public RedoAction REDO_ACTION = new RedoAction(this);
	
	private JMenuBar menuBar;
	
	private JTabbedPane workingFilesTab;
	
	private JPanel watcherPanel;
	
	private JSplitPane splitPane;
	
	private JToolBar toolbar;
	
	private JPopupMenu popupForSourceViewer;
	
	private Hashtable<String, SourceViewer> sourceViewers;
	private UIProperties uiprop;
	
	public MainPage(UIProperties uiprop){
		this.uiprop = uiprop;
		
		workingFilesTab = new JTabbedPane();
		
		watcherPanel = new JPanel();
		
		_createMenuBar();
		
		_createSplitPane();
		
		_createToolbar();
		
		_createPopupForSourceViewer();
		
		sourceViewers = new Hashtable<String, SourceViewer>();
		
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setTitle(APPLICATION_NAME);
		super.setSize(600, 600);
		super.getContentPane().setLayout(new BorderLayout());
		super.getContentPane().add(this.toolbar, BorderLayout.NORTH);
		super.getContentPane().add(splitPane, BorderLayout.CENTER);
	}
	private void _createMenuBar(){
		menuBar = new JMenuBar();
		
		JMenu menuEdit = new JMenu("편집");
		JMenuItem findReplaceItem = new JMenuItem("찾기/바꾸기");
		findReplaceItem.addActionListener(this.FIND_REPLACE_ACTION);
		menuEdit.add(findReplaceItem);
		
		JMenu menuWelcome = new JMenu("정보");
		JMenuItem makerItem = new JMenuItem("만든이");
		makerItem.addActionListener(new MakerInfoAction());
		menuWelcome.add(makerItem);
		
		menuBar.add(menuEdit);
		menuBar.add(menuWelcome);
		
		super.setJMenuBar(menuBar);
	}
	private void _createSplitPane(){
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, workingFilesTab, watcherPanel);
		splitPane.setDividerLocation(400);
	}
	private void _createToolbar(){
		toolbar = new JToolBar();
		
		JButton openButton = new JButton("Open");
		openButton.addActionListener(new OpenAction(this));
		toolbar.add(openButton);
	}
	private void _createPopupForSourceViewer(){
		this.popupForSourceViewer =new JPopupMenu();

		JMenuItem undoItem = new JMenuItem("되돌리기");
		undoItem.addActionListener(this.UNDO_ACTION);
		JMenuItem redoItem = new JMenuItem("다시하기");
		redoItem.addActionListener(this.REDO_ACTION);
		JMenuItem findReplItem = new JMenuItem("찾기/바꾸기");
		findReplItem.addActionListener(this.FIND_REPLACE_ACTION);
		
		this.popupForSourceViewer.add(undoItem);
		this.popupForSourceViewer.add(redoItem);
		this.popupForSourceViewer.add(findReplItem);
	}
	
	
	
	
	
	
	public void closeFile(String filename){
		workingFilesTab.removeTabAt(workingFilesTab.indexOfTab(filename));
		sourceViewers.remove(filename);
	}
	public void openFile(File f){
		f = f.getAbsoluteFile();
		String totalpath = f.getAbsolutePath();
		if(sourceViewers.containsKey(totalpath)){
			int res = JOptionPane.showConfirmDialog(null,  "File '" + totalpath + "' 은 이미 열려있습니다. " +
					"Close previous file and 이 파일을 열까요?", APPLICATION_NAME,JOptionPane.YES_NO_OPTION);
			if(res == JOptionPane.YES_OPTION){
				closeFile(totalpath);
			}else
				return;
		}
		SourceViewer sv = new SourceViewer(uiprop);
		sv.setPopup(this.popupForSourceViewer);
		
		sourceViewers.put(totalpath, sv);
		workingFilesTab.addTab(totalpath, sv);
		workingFilesTab.setTabComponentAt(workingFilesTab.indexOfTab(totalpath), 
				new TabComp(this, f.getName(), totalpath));
	}
	public SourceViewer getCurrentViewer(){
		return (SourceViewer)workingFilesTab.getSelectedComponent();
	}
	
	
	
	
	public static class TabComp extends JPanel{
		private String absolutePath;
		MainPage mpage;
		static Insets INSETS_DEF = new Insets(1, 1, 1, 1);
		
		public TabComp(MainPage mpage, String title, String absolutePath){
			super.setLayout(new FlowLayout());
			
			JButton closebtn = new JButton("x");
			closebtn.setMargin(INSETS_DEF);
			closebtn.setBorder(new EmptyBorder(0, 0, 0, 0));
			closebtn.addActionListener(new CloseFileTabAction(mpage, absolutePath));
			
			super.add(new JLabel(title));
			super.add(closebtn);
			this.absolutePath = absolutePath;
		}
	}
	
	
	
	public static void main(String[] argv) throws Exception{
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		new MainPage(UIProperties.getDefaultUIProperties()).setVisible(true);
	}
}
