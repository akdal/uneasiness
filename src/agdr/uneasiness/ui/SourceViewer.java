package agdr.uneasiness.ui;

import java.awt.Color;

import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.Element;
import javax.swing.event.CaretEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.CaretListener;
import javax.swing.undo.*;

public class SourceViewer extends JScrollPane implements MouseListener{
	private JTextPane textPane;
	private JTextArea lineNumberArea;
	private static String LINE_SEPARATOR = System.getProperty("line.separator");
	
	private Highlighter rootHighlighter;
	
	private UIProperties uiprop;
	
	private JPopupMenu popup;
	
	private UndoManager undoManager;
	
	
	public SourceViewer(UIProperties uiprop){
		this.uiprop = uiprop;
		
		Font font = (Font)uiprop.getValue(UIProperties.TEXT_FONT);
		this.lineNumberArea= new JTextArea();
		this.lineNumberArea.setBackground(Color.LIGHT_GRAY);
		this.lineNumberArea.setEditable(false);
		this.lineNumberArea.setText("1");
		this.lineNumberArea.setFont(font);

		this.textPane = new JTextPane();
		this.textPane.setFont(font);
		this.textPane.addMouseListener(this);
		this.textPane.getDocument().addDocumentListener(new LineNumberingDocListener());
		this.textPane.addCaretListener(new LinePainter(this.textPane, this.uiprop));
		
		this.undoManager = new UndoManager();
		this.textPane.getDocument().addUndoableEditListener(undoManager);
		
		this.rootHighlighter = this.textPane.getHighlighter();
		
		this.getViewport().add(textPane);
		this.setRowHeaderView(this.lineNumberArea);
		this.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_ALWAYS);
	};
	

	public void setPopup(JPopupMenu pm)
	{ this.popup = pm; }
	public void undo(){
		try{
			this.undoManager.undo();
		}catch(Exception e)
		{ e.printStackTrace(); }
	}
	public void redo(){
		try{
			this.undoManager.redo();
		}catch(Exception e)
		{ e.printStackTrace(); }
	}
	
	
	@Override
	public void mouseClicked(MouseEvent e) {}


	@Override
	public void mouseEntered(MouseEvent e) {}


	@Override
	public void mouseExited(MouseEvent e) {}


	@Override
	public void mousePressed(MouseEvent e) {}


	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getComponent() == textPane){
			if(e.isPopupTrigger()){
				if(this.popup != null)
					this.popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}
	}
	
	

	
	class LineNumberingDocListener implements DocumentListener{
		private int linecnt = 0;
		
		public StringBuffer getText(){
			int caretPosition = textPane.getDocument().getLength();
			Element root = textPane.getDocument().getDefaultRootElement();
			int currentlinecnt = root.getElementIndex(caretPosition) + 1;
			
			if(currentlinecnt == linecnt)
				return null;
			
			linecnt = currentlinecnt;
			
			StringBuffer textbuf = new StringBuffer("1" + LINE_SEPARATOR);
			for(int i = 2; i <= currentlinecnt; i++){
				textbuf.append(i).append(LINE_SEPARATOR);
			}
			return textbuf;
		}
		@Override
		public void changedUpdate(DocumentEvent de) {
			StringBuffer buf = getText();
			if(buf == null)
				return;
			lineNumberArea.setText(buf.toString());
		}

		@Override
		public void insertUpdate(DocumentEvent de) {
			StringBuffer buf = getText();
			if(buf == null)
				return;
			lineNumberArea.setText(buf.toString());
		}

		@Override
		public void removeUpdate(DocumentEvent de) {
			StringBuffer buf = getText();
			if(buf == null)
				return;
			lineNumberArea.setText(buf.toString());
		}
	}
	
	
	public static void main(String[] argv) throws Exception{
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		JFrame jf = new JFrame("df");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.getContentPane().add(new SourceViewer(UIProperties.getDefaultUIProperties()));
		jf.setSize(400, 400);
		jf.setVisible(true);
	}

}
